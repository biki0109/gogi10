package client

import (
	"context"

	"git.begroup.team/platform-core/kitchen/l"
	"git.begroup.team/platform-transport/gogi10/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/balancer/roundrobin"
)

type Gogi10Client struct {
	pb.Gogi10Client
}

func NewGogi10Client(address string) *Gogi10Client {
	conn, err := grpc.DialContext(context.Background(), address,
		grpc.WithInsecure(),
		grpc.WithBalancerName(roundrobin.Name),
	)

	if err != nil {
		ll.Fatal("Failed to dial Gogi10 service", l.Error(err))
	}

	c := pb.NewGogi10Client(conn)

	return &Gogi10Client{c}
}
